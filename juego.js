document.addEventListener('DOMContentLoaded',()=>{
    //opciones para las cartas
    const arregloTarjetas=[
        {
            name:'Charmander', img:'Imagenes/Charmander.png'
        },
        {
            name:'Bulbasaur', img:'Imagenes/Bulbasaur.png'
        },
        {
            name:'Pikachu', img:'Imagenes/Pikachu.png'
        },
        {
            name:'Squirtle', img:'Imagenes/Squirtle.png'
        },      
        {
            name:'Eevee', img:'Imagenes/Eevee.png'
        },
        {
            name:'Mew', img:'Imagenes/Mew.png'
        },
        {
            name:'Charmander', img:'Imagenes/Charmander.png'
        },
        {
            name:'Bulbasaur', img:'Imagenes/Bulbasaur.png'
        },
        {
            name:'Pikachu', img:'Imagenes/Pikachu.png'
        },
        {
            name:'Squirtle', img:'Imagenes/Squirtle.png'
        },
        {
            name:'Eevee', img:'Imagenes/Eevee.png'
        },
        {
            name:'Mew', img:'Imagenes/Mew.png'
        }    
    ]
    arregloTarjetas.sort(()=>0.5 - Math.random())//para que las posiciones de las tarjetas sean aleatoreas

    const grid = document.querySelector('.grid')
    const resultado=document.querySelector('#resultado')
    let tarjetasSeleccionadas=[]
    let IDTarjetaSelecciondas=[]
    let tarjetaGanadora=[]

    // funcion para crear el tablero
    function crearTablero(){
        for (let i=0;i<arregloTarjetas.length;i++){
            const tarjeta=document.createElement('img')
            tarjeta.setAttribute('src','Imagenes/Pokeball.png')
            tarjeta.setAttribute('data-id', i)
            tarjeta.addEventListener('click', voltearTarjeta)
            grid.appendChild(tarjeta)
        }
    }
    //funcion para verificar ganador
    function verificarGanador(){
        const tarjeta=document.querySelectorAll('img')
        const opcion1ID=IDTarjetaSelecciondas[0]
        const opcion2ID=IDTarjetaSelecciondas[1]

        if (opcion1ID==opcion2ID){ //si el usuario selecciona la misma imagen
            tarjeta[opcion1ID].setAttribute('src','Imagenes/Pokeball.png')
            tarjeta[opcion2ID].setAttribute('src','Imagenes/Pokeball.png')
            alert ('Seleccionaste la misma imagen')
        }
        else if (tarjetasSeleccionadas[0]===tarjetasSeleccionadas[1]){
            alert('Encontraste un par')
            tarjeta[opcion1ID].setAttribute('src','Imagenes/Vacio.png')
            tarjeta[opcion2ID].setAttribute('src','Imagenes/Vacio.png')
            tarjeta[opcion1ID].removeEventListener('click',voltearTarjeta)
            tarjeta[opcion2ID].removeEventListener('click',voltearTarjeta)
            tarjetaGanadora.push(tarjetasSeleccionadas)
        }else{
            tarjeta[opcion1ID].setAttribute('src','Imagenes/Pokeball.png')
            tarjeta[opcion2ID].setAttribute('src','Imagenes/Pokeball.png')
            alert('Intenta otravez')
        }
        tarjetasSeleccionadas=[]
        IDTarjetaSelecciondas=[]
        resultado.textContent=tarjetaGanadora.length
        if (tarjetaGanadora.length===arregloTarjetas.length/2)
            resultado.textContent=' Felicidades, encontraste todos los pares!'
    }
    //Funcion para voltear la tarjeta
    function voltearTarjeta(){
        let IDtarjeta=this.getAttribute('data-id')
        tarjetasSeleccionadas.push(arregloTarjetas[IDtarjeta].name)
        IDTarjetaSelecciondas.push(IDtarjeta)
        this.setAttribute('src',arregloTarjetas[IDtarjeta].img)
        if (tarjetasSeleccionadas.length==2)
            setTimeout(verificarGanador,500)
    }

    crearTablero()
})